# Weather Application

Prerequisite
- Install Node.js

Run
- Execute 'npm run start' to run 

Application Feature
- Enter city / country keyword to search weather of the location.
- If search field is blank, application will return weather information of current location.
- Click the temperature unit to convert the temperature unit between celcius and fahrenheit.
- Select interval for update interval of the tile created.
- Each tile will refresh individually according to interval selected.
- Each location can only add once into the tile list.
- Click remove to remove tile from the list.
- Application handle responsively from web to mobile resolution.



