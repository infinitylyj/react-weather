import React, { Component } from 'react';
import OpacityBackgroundImg from './OpacityBackgroundImg';
import WeatherWidget from './WeatherWidget';
import './App.css';




class App extends Component {
  constructor(){
    super();
    this.state={
      about:false
    };
}


  render() {

    return (
      <div className="main">
        <OpacityBackgroundImg imgSrc={'weather-background.jpg'} opacity={0.2} />
        <div className='app'>
          <h1 className='app-header'>Weather Application</h1>
          <WeatherWidget />
        </div>
      </div>
    );
  }
}

export default App;
