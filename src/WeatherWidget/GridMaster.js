import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import GridWidget from './GridWidget'
import './GridWidget.css';

class GridMaster extends Component {
  render(){
    let {companyList, removeItem} = this.props

    return (
      <div className="root-fit">
      <Grid container spacing={3}>
        {(companyList).map(co => 
          <Grid item xs>
            <Paper className='paper'><GridWidget name={co.name} temp={co.temp} weather={co.weather} lastUpdateTime={co.lastUpdateTime} removeItem={removeItem}/></Paper>
          </Grid>
        )}
      </Grid>
    </div>
    );
  }
}

export default GridMaster;
