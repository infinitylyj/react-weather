const API_URL = 'http://api.openweathermap.org/data/2.5/weather';
const API_KEY = 'ef2b4e139224fcc273d0127ed0b221f8';

export const kelvinConverter = {
  toCelcius:k=>Math.round(k-273),
  toFarenheight:k=>Math.round((k-273)*9/5+32)
}

export const fetchWeather = query => {
  return fetch(`${API_URL}?${query}&APPID=${API_KEY}`)
  .then(res=>{
    return res.json();
  }).then(data=>{
    if(data.cod===200){
      return data
    }else{
      throw(new Error(data.errorMsg||"Something went wrong."))
    }
  })
  }

  export const fetchWeatherByLocation = query => {
    return fetch(`${API_URL}?q=${query}&APPID=${API_KEY}`)
    .then(res=>{
      return res.json();
    }).then(data=>{
      if(data.cod===200){
        return data
      }else{
        throw(new Error(data.errorMsg||"Something went wrong."))
      }
    })
    }



