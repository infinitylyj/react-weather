import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import OpenWeatherIcon from './OpenWeatherIcon'
import {kelvinConverter} from './_utils'
import './GridWidget.css';

class GridWidget extends Component {

  render(){
    let {name,temp,weather,lastUpdateTime, removeItem} = this.props

    return (
      <div className="root">
        <Paper className="paper">
          <Grid container spacing={2} className="container-fit">
            <Grid item>
            <OpenWeatherIcon weather={weather} />
            </Grid>
            <Grid item xs={12} sm container>
              <Grid item xs container direction="column" spacing={2}>
                <Grid item xs>
                  <Typography gutterBottom variant="h4">
                    {name}
                  </Typography>
                  <Typography variant="h6" gutterBottom>
                    {weather.description[0].toUpperCase()+weather.description.substring(1)}
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography variant="body2" style={{ cursor: 'pointer' }} >
                    <Button variant="contained" color="primary" onClick={() => removeItem(name)}>Remove</Button>
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography variant="body2">{lastUpdateTime}</Typography>
                </Grid>
              </Grid>
              {/* <Grid item>
                <Typography variant="h6">{kelvinConverter.toCelcius(temp)}C</Typography>
              </Grid> */}
            </Grid>
          </Grid>
        </Paper>
      </div>
    );
  }
}

export default GridWidget;
