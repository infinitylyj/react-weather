import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import OpenWeatherIcon from './OpenWeatherIcon'
import TemperatureConverter from './TemperatureConverter';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import SearchBar from './SearchBar';

class SearchMaster extends Component {
  render(){
    let {weather,
      transitioning,
      name,
      country,
      temp,
      interval,
      intervalChangeEvent,
      addItemEvent,
      setWeatherDataEvent,
      clearWeatherDataEvent} = this.props

    return (
      weather?
      <div style={{height:500}} className={`weather-panel${transitioning?' transitioning':''}`}>
      <div className='weather-location'>{name}, {country}</div>
      <div className='weather-description'>
        {weather.description[0].toUpperCase()+weather.description.substring(1)}
      </div>
      <div style={{display:'flex', alignItems:'center',width:300}}>
        <OpenWeatherIcon weather={weather} />
        <TemperatureConverter temp={temp} />
      </div>
      <FormControl className="formControl theme">
          <InputLabel className="theme" shrink htmlFor="age-native-label-placeholder">
            Update Interval
          </InputLabel>
          <Select className="theme" 
            value={interval} 
            onChange={intervalChangeEvent}>
            <MenuItem value={1}>1s</MenuItem>
            <MenuItem value={5}>5s</MenuItem>
            <MenuItem value={10}>10s</MenuItem>
          </Select>
          <FormHelperText>Label + placeholder</FormHelperText>
        </FormControl>
        <Button variant="contained" color="primary" onClick={() => addItemEvent(interval)}>Add into list</Button>
      <i className='weather-search-button fas fa-redo' onClick={clearWeatherDataEvent}></i>
     </div>
      :
        <SearchBar 
         onFound={setWeatherDataEvent}
         transitioning={transitioning}
       />
    );
  }
}

export default SearchMaster;
