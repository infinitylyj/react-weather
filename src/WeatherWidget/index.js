import React, { Component, Fragment } from 'react';
import {fetchWeather} from './_utils';
import GridMaster from './GridMaster';
import SearchMaster from './SearchMaster';
import './index.css'

export default class WeatherWidget extends Component{
  constructor(){
    super();
    this.state = {
      name:null,
      country:null,
      temp:null,
      weather:null,
      transitioning:false,
      companyList : [],
      interval:5
    }
    this.setWeatherDataEvent = this.setWeatherDataEvent.bind(this);
    this.clearWeatherDataEvent = this.clearWeatherDataEvent.bind(this);
    this.intervalChangeEvent = this.intervalChangeEvent.bind(this);

  }

  addItemEvent = interval => {
    var foundIndex = this.state.companyList.findIndex(x => x.name == this.state.name);
    if(foundIndex == -1){
      this.setState({
        companyList:[...this.state.companyList, { "name": this.state.name, "temp":this.state.temp,"weather":this.state.weather, "lastUpdateTime":new Date().toLocaleString()}]
      });
      let query = `q=${this.state.name}`;
      setInterval(() => {
        this.refreshListView(query)
      }, interval*1000);
      this.clearWeatherDataEvent();
    }else{
      //alert exist
    }
  }

  removeItem = name => {
    var foundIndex = this.state.companyList.findIndex(x => x.name == name);
    this.state.companyList.splice(foundIndex, 1);
    this.setState({
      companyList:this.state.companyList
    })
  }

  refreshListView(search){
    fetchWeather(search)
    .then(res=>{
      var foundIndex = this.state.companyList.findIndex(x => x.name == res.name);
      var d = new Date();
      this.state.companyList[foundIndex] = { "name": res.name,  "temp":res.main.temp,"weather":res.weather[0], "lastUpdateTime":d.toLocaleString()};
      this.setState({
        companyList:this.state.companyList
      })
    })
  }

  setWeatherDataEvent(data){
    this.setState({transitioning:true});
    setTimeout(()=>{
      this.setState({
        name:data.name,
        country:data.sys.country,
        temp:data.main.temp,
        weather:data.weather[0],
        transitioning:false,
      });
    },400);
  }

  intervalChangeEvent(e){
    this.setState({
      interval:e.target.value
    });
  }

  clearWeatherDataEvent(){
    this.setState({transitioning:true});
    setTimeout(()=>{
      this.setState({
        name:null,
        country:null,
        temp:null,
        weather:null,
        transitioning:false,
        interval:5
      })
    },400);
  }

  render(){
    return(
      <Fragment>
        <SearchMaster 
          weather = {this.state.weather}
          transitioning = {this.state.transitioning}
          name = {this.state.name}
          country = {this.state.country}
          temp = {this.state.temp}
          interval = {this.state.interval}
          intervalChangeEvent = {this.intervalChangeEvent}
          addItemEvent = {this.addItemEvent}
          setWeatherDataEvent = {this.setWeatherDataEvent}
          clearWeatherDataEvent = {this.clearWeatherDataEvent}
        />
        <GridMaster 
          companyList = {this.state.companyList} 
          removeItem = {this.removeItem}
        />
      </Fragment>
    )
  }
}
